# TGws-Chat

#### 项目介绍
基于Vue3+Golang的websocket多人通信系统，采用前后端分离，支持发送轻量级别的信息。可通过输入websocket地址一键连接至对应的聊天室并实时观察到聊天室的用户在线情况。

#### 支持功能
1. 可视化修改默认连接地址
2. 实时展示所连接的主机数量（默认后台的/ws地址支持，其它地址需要自己实现）
3. 在同一局域网中不同主机设备可相互聊天（手机或电脑等）


#### 涉及技术
前端
1. Vue3
2. NanoID
3. Element-Plus
4. Cookie

后端
1. Golang
2. Websocket

配置完成后即可通过输入 localhost:8080/login 进入页面

#### 效果展示 :dizzy: 
![输入图片说明](display.png)